module ApplicationHelper
    
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def show_errors(object, field_name)
    if object.errors.any?
        object.errors.full_messages_for(field_name).first
    end
  end 
end
